import math
from tokenize import Double


def calc(a, b, action):
    try: 
        if action in ('+', '-', '*', '/', 'celi', 'floor', 'sin', 'cos','**','//','S','P'):
            if action == '+':
                print("%.2f" % (a+b))

            elif action == 'celi':
                print(math.ceil(a))
            
            elif action == 'floor':
                print(math.floor(a))
            
            elif action == 'sin':
                print(math.sin(a))
            
            elif action == 'cos':
                print(math.cos(a))
            
            elif action == '-':
                print((a-b))
            
            elif action == '*':
                print((a*b))
            
            elif action == '%':
                print(a%b)
            
            elif action == '//':
                print((a//b))

            elif action == '**':
                print(a**b)

            elif action == 'S':
                c = float(input("Р’РІРµРґРёС‚Рµ c: ")) 
                print(S(a, b, c))

            elif action == 'P':
                c = float(input("Р’РІРµРґРёС‚Рµ c: ")) 
                print(P(a, b, c))

            elif action == '/':
                if b != 0:
                    print( (a/b))
                else:
                    print("РћС€РёР±РєР° РґРµР»РµРЅРёСЏ")
        
    except KeyError as e:
        raise ValueError('РќРµРёР·РІРµСЃС‚РЅРѕРµ РґРµР№СЃС‚РІРёРµ: {}'.format(e.args[0]))


def stringInfo(s):
    print('РљРѕР»РёС‡РµСЃС‚РІРѕ Р·Р°РїСЏС‚С‹С… РІ СЃС‚СЂРѕРєРµ ' , (s.count(',')))
    print('РљРѕР»РёС‡РµСЃС‚РІРѕ СЃРёРјРІРѕР»РѕРІ РїСЂРѕР±РµР»Р° РІ СЃС‚СЂРѕРєРµ: ' , (s.count(' ')))
    print("РљРѕР»РёС‡РµСЃС‚РІРѕ СЃС‚СЂРѕРє: ", (len(s)))
    
P=lambda n1,n2, n3: 4*(n1 + n2 + n3)
S=lambda n1,n2, n3: 2*(n1*n2+n1*n3+n2*n3)

def matx(col, row, start, step):
    mas = []
    x = start
    for i in range(col):
        mas.append([])
        for j in range(row):
            mas[i].append(x)
            x += step
    
    return mas

def stringInfoUI():
    s = input("Р’РІРµРґРёС‚Рµ СЃС‚СЂРѕРєСѓ: ")
    stringInfo(s)


def calcUI():
    a = float(input("Р’РІРµРґРёС‚Рµ a: ")) 
    action = input("Р’С‹Р±РµСЂРёС‚Рµ РґРµР№СЃС‚РІРёРµ (+ (СЃР»РѕР¶РµРЅРёРµ), - (РІС‹С‡РёС‚Р°РЅРёРµ), * (СѓРјРЅРѕР¶РµРЅРёРµ), / (РґРµР»РµРЅРёРµ), % (РјРѕРґСѓР»СЊ), // (РґРµР»РµРЅРёРµ Р±РµР· РѕСЃС‚Р°С‚РєР°), ** (СЃС‚РµРїРµРЅСЊ), sin (СЃРёРЅСѓСЃ), cos (РєРѕСЃРёРЅСѓСЃ), S (РїР»РѕС‰Р°РґСЊ), P (РїРµСЂРёРјРµС‚СЂ))")
    if action in ('+', '-', '*', '/', '%', '//', '**','S','P'):
        b = float(input("Р’РІРµРґРёС‚Рµ b: "))
    else:
        b = 0
    calc(a, b, action)


def matxUI():
    col = int(input("Р’РІРµРґРёС‚Рµ РєРѕР»РёС‡РµСЃС‚РІРѕ СЃС‚СЂРѕРє: "))
    row = int(input("Р’РІРµРґРёС‚Рµ РєРѕР»РёС‡РµСЃС‚РІРѕ СЃС‚РѕР»Р±С†РѕРІ: "))
    start = int(input("Р’РІРµРґРёС‚Рµ РїРµСЂРІРѕРµ С‡РёСЃР»Рѕ: "))
    step = int(input("Р’РІРµРґРёС‚Рµ С€Р°Рі: "))
    A = matx(col, row, start, step)
    for r in A:
        print(*r)



def mainUI():
    while True:
        action = input("Р’С‹Р±РµСЂРёС‚Рµ Р·Р°РґР°РЅРёРµ (1 - РєР°Р»СЊРєСѓР»СЏС‚РѕСЂ, 2 - РїРѕРґСЃС‡РµС‚ СЃС‚СЂРѕРєРё, 3 - РјР°С‚СЂРёС†Р°, 4 - РІС‹С…РѕРґ)")
        if action == '1':
            calcUI()
        elif action == '2':
            stringInfoUI()
        elif action == '3':
            matxUI()
        elif action == '4':
            break

mainUI()











